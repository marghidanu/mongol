#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Perl::Critic (
	-severity => 1,
	-exclude => [
		'strict', 'warnings', 'constant',
		'RequireVersionVar', 'RequireCarping', 'RequirePodSections', 'RequireTidyCode',

		'ProhibitPostfixControls', 'ProhibitLongChainsOfMethodCalls', 'ProhibitPunctuationVars',
		'ProhibitParensWithBuiltins', 'ProhibitBuiltinHomonyms', 'ProhibitUnusedPrivateSubroutines'
	],
);

all_critic_ok();
